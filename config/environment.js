'use strict';

module.exports = function(environment) {
  let ENV = {
    modulePrefix: 'github-org-helper',
    environment,
    rootURL: '/',
    locationType: 'hash',
    EmberENV: {

      EXTEND_PROTOTYPES: {
        Date: false
      }

    },

    APP: {
      // Here you can pass flags/options to your application instance
      // when it is created
    },

    github: {
      pagination: {
        branches: 100,
        repositories: 100
      }
    }

  };

  if (environment === 'development') {


    ENV.APP.LOG_RESOLVER = true;
    ENV.APP.LOG_ACTIVE_GENERATION = true;
    ENV.APP.LOG_TRANSITIONS = true;
    ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    ENV.APP.LOG_VIEW_LOOKUPS = true;

    ENV['ember-cli-mirage'] = {
      enabled: false
    };

  }

  if (environment === 'test') {

    ENV.locationType = 'none';

    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.rootElement = '#ember-testing';
    ENV.APP.autoboot = false;

  }

  if (environment === 'production') {

    // here you can enable a production-specific feature

  }

  return ENV;
};
