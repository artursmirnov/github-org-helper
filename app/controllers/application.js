import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import { or } from '@ember/object/computed';

export default Controller.extend({

  organization: service(),

  errorMessage: '',
  isLoading: false,

  showLoadingIndication: or(
    'isLoading',
    'organization.changeOrganization.isRunning',
    'organization.loadRepositories.isRunning'
  )

});
