import Controller from '@ember/controller';
import { computed } from '@ember/object';
import { or } from '@ember/object/computed';
import { inject as service } from '@ember/service';
import { ALL_LANGUAGES } from 'github-org-helper/services/repository-filter';

export default Controller.extend({
  filter: service('repository-filter'),
  organization: service(),

  showLoading: or('organization.loadRepositories.isRunning', 'organization.changeOrganization.isRunning'),

  repositories: computed(
    'filter.{showPublic,showPrivate,showLanguage}',
    'organization.repositories.@each.{language,private}',
    function() {
      const { showPublic, showPrivate, showLanguage } = this.filter;
      const repositories = this.organization.get('repositories') || [];
      return repositories.compact().filter(repository => {
        const fitsLanguage = showLanguage === ALL_LANGUAGES || repository.language === showLanguage;
        const fitsPublic = showPublic && !repository.private;
        const fitsPrivate = showPrivate && repository.private;
        return fitsLanguage && (fitsPublic || fitsPrivate);
      })
    }
  )
});
