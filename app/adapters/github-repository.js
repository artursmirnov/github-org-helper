import GithubRepositoryAdapter from 'ember-data-github/adapters/github-repository';
import NoCache from 'github-org-helper/mixins/adapters/no-cache';

export default GithubRepositoryAdapter.extend(NoCache, {
});
