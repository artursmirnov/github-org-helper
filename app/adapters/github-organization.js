import GithubOrganizationAdapter from 'ember-data-github/adapters/github-organization';
import NoCache from 'github-org-helper/mixins/adapters/no-cache';

export default GithubOrganizationAdapter.extend(NoCache, {
});
