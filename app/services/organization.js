import Service from '@ember/service';
import { reads, not } from '@ember/object/computed';
import { inject as service } from '@ember/service';
import { task } from 'ember-concurrency';

export default Service.extend({

  router: service(),
  store: service(),

  model: null,

  isEmpty: not('model'),

  id: reads('model.id'),
  name: reads('model.name'),
  repositories: reads('model.repositories'),

  change(id = '') {
    return this.changeOrganization.perform(id);
  },

  changeOrganization: task(function* (id) {
    yield this.router.transitionTo('/');
    if (id) {
      yield this.router.transitionTo('organizations.organization.repositories', id);
    }
  }).drop(),

  loadRepositories: task(function* () {
    this.store.unloadAll('github-repository');
    yield this.get('model.repositories');
  }).drop()

});
