import Service from '@ember/service';
import { computed } from '@ember/object';
import Evented from '@ember/object/evented';

export const ALL_LANGUAGES = 'All languages';
export const CHANGE_EVENT = 'change';

export const DEFAULT_FILTERS = {
  showPrivate: false,
  showPublic: true,
  showLanguage: ALL_LANGUAGES
}

let showPublic = DEFAULT_FILTERS.showPublic;
let showPrivate = DEFAULT_FILTERS.showPrivate;
let showLanguage = DEFAULT_FILTERS.showLanguage;

export default Service.extend(Evented, {

  showPublic: computed({
    get() {
      return showPublic;
    },
    set(key, value) {
      showPublic = value;
      this.trigger(CHANGE_EVENT);
      return value;
    }
  }),

  showPrivate: computed({
    get() {
      return showPrivate;
    },
    set(key, value) {
      showPrivate = value;
      this.trigger(CHANGE_EVENT);
      return value;
    }
  }),

  showLanguage: computed({
    get() {
      return showLanguage;
    },
    set(key, value) {
      showLanguage = value;
      this.trigger(CHANGE_EVENT);
      return value;
    }
  }),

  reset() {
    this.setProperties(DEFAULT_FILTERS);
  }

});
