import GithubSessionService from 'ember-data-github/services/github-session';
import { computed } from '@ember/object';
import { bool } from '@ember/object/computed';
import { storageFor } from 'ember-local-storage';

export default GithubSessionService.extend({

  auth: storageFor('auth'),

  githubAccessToken: computed('auth.key', {
    get() {
      return this.get('auth.key');
    },
    set(key, value) {
      this.set('auth.key', value);
      return value;
    }
  }),

  isAuthenticated: bool('githubAccessToken')

});
