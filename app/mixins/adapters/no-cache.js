import Mixin from '@ember/object/mixin';

export default Mixin.create({

  ajaxOptions() {
    const options = this._super(...arguments) || {};
    options.cache = false;
    return options;
  }

});
