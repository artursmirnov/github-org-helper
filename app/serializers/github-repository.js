import GithubRepositorySerializer from 'ember-data-github/serializers/github-repository';
import config from 'github-org-helper/config/environment';

const BRANCHES_PER_PAGE_LIMIT = config.github.pagination.branches;

export default GithubRepositorySerializer.extend({

  normalize() {
    const normalized = this._super(...arguments);
    try {
      normalized.data.relationships.branches.links.related += `?per_page=${BRANCHES_PER_PAGE_LIMIT}`
    } catch(e) { /* silent error */ }
    return normalized;
  }

});
