import GithubOrganizationSerializer from 'ember-data-github/serializers/github-organization';
import config from 'github-org-helper/config/environment';

const REPOSITORIES_PER_PAGE_LIMIT = config.github.pagination.repositories;

export default GithubOrganizationSerializer.extend({

  normalize() {
    const normalized = this._super(...arguments);
    try {
      normalized.data.relationships.repositories.links.related += `?per_page=${REPOSITORIES_PER_PAGE_LIMIT}`
    } catch(e) { /* silent error */ }
    return normalized;
  }

});
