import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';

export default Route.extend({
  organization: service(),

  beforeModel(transition) {
    if (transition.targetName === this.routeName) {
      if (this.organization.name) {
        this.transitionTo('organizations.organization', this.organization.name);
      } else {
        this.transitionTo('/');
      }
    }
  }

});
