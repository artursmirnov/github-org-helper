import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';

export default Route.extend({

  organization: service(),
  filter: service('repository-filter'),

  model({ name }) {
    return this.store.findRecord('github-organization', name);
  },

  afterModel(model) {
    this.organization.set('model', model);
    this.filter.reset();
  },

  redirect() {
    this.transitionTo('organizations.organization.repositories');
  },

  deactivate() {
    this.organization.set('model', null);
  }

});
