import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';

export default Route.extend({

  organization: service(),

  model() {
    return this.organization.loadRepositories.perform();
  }

});
