import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';

export default Route.extend({
  organization: service(),
  filter: service('repository-filter'),
  github: service('github-session'),

  clearError() {
    this.controller.set('errorMessage', '');
  },

  setLoading(value) {
    this.controller.set('isLoading', !!value);
  },

  actions: {

    reloadOrganization() {
      this.clearError();
      if (!this.organization.isEmpty) {
        this.setLoading(true);
        this.store.findRecord('github-organization', this.organization.id, { reload: true })
          .then((organization) => {
            this.organization.set('model', organization);
            return this.organization.loadRepositories.perform();
          })
          .then(() => {
            this.set('filter.showPrivate', this.github.isAuthenticated);
          })
          .catch(error => {
            this.send('error', error);
          })
          .finally(() => {
            this.setLoading(false);
          });
      }
    },

    willTransition() {
      this.clearError();
    },

    error(error) {
      if (error.isAdapterError) {
        const httpError = error.errors[0] || {};
        switch (httpError.status) {
          case "404":
            this.controller.set('errorMessage', 'Organization not found');
            break;
          case "403":
            this.controller.set('errorMessage', 'Github API rate limit for unauthorized requests exceeded. Use Github Auth Token for authorization or wait a bit, it should reset within an hour.');
            break;
          case "401":
            this.controller.set('errorMessage', 'Github Access Token seems to be invalid.');
            break;
          default:
            this.controller.set('errorMessage', 'Github API is temporary unavailable');
        }
      } else {
        this.controller.set('errorMessage', 'Something went wrong :(');
      }
    }

  }

});
