import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { or } from '@ember/object/computed';

export default Component.extend({
  classNames: [ 'organization-name' ],

  organization: service(),

  isEditMode: false,

  name: '',

  buttonTitle: or('organization.name', 'organization.id'),

  enableEditMode() {
    this.set('isEditMode', true);
    this.set('name', this.organization.id || '');
  },

  disableEditMode() {
    this.set('isEditMode', false);
  },

  actions: {

    enableEditMode() {
      this.enableEditMode();
    },

    changeOrganization() {
      this.organization.change(this.name);
      this.disableEditMode();
    },

    selectInput({ target }) {
      target.select();
    },

    closeDialog() {
      this.disableEditMode();
    }

  }

});
