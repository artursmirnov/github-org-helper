import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { reads, and, not } from '@ember/object/computed';
import { next } from '@ember/runloop';
import { CHANGE_EVENT } from 'github-org-helper/services/repository-filter';

export default Component.extend({
  classNames: [ 'auth-form' ],

  github: service('github-session'),
  filter: service('repository-filter'),

  key: reads('github.githubAccessToken'),

  needsAuthentication: not('github.isAuthenticated'),
  isMenuOpen: and('needsAuthentication', 'filter.showPrivate'),

  onAuthorize() {},

  openMenu() {},

  didReceiveAttrs() {
    this.openMenuIfNecessary();
  },

  init() {
    this._super(...arguments);
    this.filter.on(CHANGE_EVENT, this.handleFilterChange.bind(this));
  },

  openMenuIfNecessary() {
    if (this.isMenuOpen) {
      this.openMenu();
    }
  },

  handleFilterChange() {
    next(() => {
      this.openMenuIfNecessary();
      if (!this.github.isAuthenticated && this.filter.showPrivate) {
        this.set('filter.showPrivate', false);
      }
    });
  },

  actions: {

    submit(closeMenu) {
      this.github.set('githubAccessToken', this.key);
      closeMenu();
      this.onAuthorize(this.key);
    },

    selectContent({ target }) {
      target.select();
    },

    registerAPI(publicAPI) {
      const openMenu = publicAPI && publicAPI.actions && publicAPI.actions.open;
      if (openMenu && this.openMenu !== openMenu) {
        this.openMenu = openMenu;
        this.openMenuIfNecessary();
      }
    }

  }
});
