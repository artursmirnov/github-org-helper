import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';
import { reads } from '@ember/object/computed';
import { isArray } from '@ember/array';
import { ALL_LANGUAGES } from 'github-org-helper/services/repository-filter';

export default Component.extend({
  classNames: [ 'repository-filters' ],

  organization: service(),
  filter: service('repository-filter'),

  repositories: reads('organization.repositories'),

  languages: computed('repositories.@each', function() {
    let languages = [];
    const repositories = this.get('repositories');
    if (isArray(repositories)) {
      languages = repositories.toArray().compact().mapBy('language').uniq().compact();
    }
    return [ ALL_LANGUAGES, ...languages ];
  }),

});
