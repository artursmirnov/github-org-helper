import Component from '@ember/component';
import { computed } from '@ember/object';

export default Component.extend({
  classNames: [ 'repo-branch' ],

  branch: null,

  branchUrl: computed('branch.name', 'repository.htmlUrl', function() {
    return `${this.get('repository.htmlUrl')}/tree/${this.get('branch.name')}`;
  })

});
