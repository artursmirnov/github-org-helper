import Component from '@ember/component';

export default Component.extend({
  classNames: [ 'app-header' ],

  onSetAuthKey() {},

  actions: {

    setAuthKey(key) {
      this.onSetAuthKey(key);
    }

  }

});
