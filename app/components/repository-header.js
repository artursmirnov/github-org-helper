import Component from '@ember/component';
import { computed } from '@ember/object';
import { equal } from '@ember/object/computed';

const EXPANDED_ICON = "keyboard_arrow_up";
const COLLAPSED_ICON = "keyboard_arrow_down";

export default Component.extend({
  classNames: [ 'repository-header' ],

  expanded: false,
  repoistory: null,

  icon: computed('expanded', function() {
    return this.expanded ? EXPANDED_ICON : COLLAPSED_ICON;
  }),

  isSingular: equal('repository.branches.length', 1),

  actions: {

    openRepo() {
      window.open(this.get('repository.htmlUrl'))
    }

  }

});
