import { module, test } from 'qunit';
import { visit, currentURL, click, fillIn } from '@ember/test-helpers';
import { setupApplicationTest } from 'ember-qunit';

import {
  ORGANIZATION_NAME,
  ORGANIZATION_NAME_POPUP,
  ORGANIZATION_NAME_INPUT,
  ORGANIZATION_NAME_SUBMIT,
  ORGANIZATION_NAME_CANCEL,
} from '../helpers/selectors';

module('Acceptance | user can select organization', function(hooks) {
  setupApplicationTest(hooks);

  hooks.beforeEach(async () => {
    await visit('/');
  });

  test('Organization name control is available', (assert) => {
    assert.dom(ORGANIZATION_NAME).exists();
  });

  module('Organization name popup', function(hooks) {

    hooks.beforeEach(async () => {
      await click(ORGANIZATION_NAME);
    });

    test('Click on organization name shows popup', (assert) => {
      assert.dom(ORGANIZATION_NAME_POPUP).exists();
    });

    test('Cancel closes popup', async (assert) => {
      await click(ORGANIZATION_NAME_CANCEL);
      assert.dom(ORGANIZATION_NAME_POPUP).doesNotExist();
    });

    test('Submit redirects to organization page', async (assert) => {
      await fillIn(ORGANIZATION_NAME_INPUT, 'github');
      await click(ORGANIZATION_NAME_SUBMIT);
      assert.notEqual(currentURL().indexOf('github'), -1);
    });

  });

});
