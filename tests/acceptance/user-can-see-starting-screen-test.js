import { module, test } from 'qunit';
import { visit, currentURL } from '@ember/test-helpers';
import { setupApplicationTest } from 'ember-qunit';

import {
  NO_DATA,
  ORGANIZATION_NAME,
  AUTH_FORM_TRIGGER,
  REPOSITORY_FILTERS
} from '../helpers/selectors';

module('Acceptance | user can see starting screen', function(hooks) {
  setupApplicationTest(hooks);

  hooks.beforeEach(async function() {
    await visit('/');
  })

  test('visiting /', (assert) => {
    assert.equal(currentURL(), '/');
  });

  module('Page', () => {

    test('has select organization message', (assert) => {
      assert.dom(NO_DATA).hasText('Please select a Github Organization');
    });

    test('has organization name control', (assert) => {
      assert.dom(ORGANIZATION_NAME).exists();
    });

    test('has auth form trigger', (assert) => {
      assert.dom(AUTH_FORM_TRIGGER).exists();
    });

    test('has filters', (assert) => {
      assert.dom(REPOSITORY_FILTERS).exists();
    })

  });

});
