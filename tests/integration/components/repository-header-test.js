import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';

import { REPOSITORY_HEADER } from '../../helpers/selectors';

module('Integration | Component | repository-header', function(hooks) {
  setupRenderingTest(hooks);

  test('it renders', async function(assert) {
    await render(hbs`{{repository-header}}`);

    assert.dom(REPOSITORY_HEADER).exists();
  });
});
