import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';

import { AUTH_FORM_TRIGGER } from '../../helpers/selectors';

module('Integration | Component | auth-form', function(hooks) {
  setupRenderingTest(hooks);

  test('it renders', async function(assert) {
    await render(hbs`{{auth-form}}`);

    assert.dom(AUTH_FORM_TRIGGER).exists();
  });
});
