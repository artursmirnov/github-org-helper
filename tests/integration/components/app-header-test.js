import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';

import { APP_HEADER } from '../../helpers/selectors';

module('Integration | Component | app-header', function(hooks) {
  setupRenderingTest(hooks);

  test('it renders', async function(assert) {
    await render(hbs`{{app-header}}`);

    assert.dom(APP_HEADER).exists();
  });
});
