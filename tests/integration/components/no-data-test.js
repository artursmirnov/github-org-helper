import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';

import { NO_DATA } from '../../helpers/selectors';

const TEXT = 'Test text for message';

module('Integration | Component | no-data', function(hooks) {
  setupRenderingTest(hooks);

  hooks.beforeEach(async function() {
    this.set('message', TEXT);
    await render(hbs`{{no-data message=message }}`);
  });

  test('it renders', (assert) => {
    assert.dom(NO_DATA).exists();
  });

  test('it has correct message', (assert) => {
    assert.dom(NO_DATA).hasText(TEXT);
  });

});
