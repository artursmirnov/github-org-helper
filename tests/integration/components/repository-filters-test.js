import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';

import { REPOSITORY_FILTERS } from '../../helpers/selectors';

module('Integration | Component | repository-filters', function(hooks) {
  setupRenderingTest(hooks);

  test('it renders', async function(assert) {
    await render(hbs`{{repository-filters}}`);

    assert.dom(REPOSITORY_FILTERS).exists();
  });
});
