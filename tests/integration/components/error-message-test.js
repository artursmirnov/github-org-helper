import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import { faker } from 'ember-cli-mirage';

import {
  ERROR_MESSAGE,
  ERROR_MESSAGE_TITLE,
  ERROR_MESSAGE_IMAGE,
  ERROR_MESSAGE_TEXT
} from '../../helpers/selectors';

const TEXT = faker.lorem.sentence();

module('Integration | Component | error-message', function(hooks) {
  setupRenderingTest(hooks);

  hooks.beforeEach(async function() {
    this.set('message', TEXT);
    await render(hbs`{{error-message message=message }}`);
  })

  test('it renders', (assert) => {
    assert.dom(ERROR_MESSAGE).exists();
  });

  test('it has valid title', (assert) => {
    assert.dom(ERROR_MESSAGE_TITLE).exists();
    assert.dom(ERROR_MESSAGE_TITLE).hasText('Ooops...');
  });

  test('it has an image', (assert) => {
    assert.dom(ERROR_MESSAGE_IMAGE).exists();
  });

  test('it has a proper message', (assert) => {
    assert.dom(ERROR_MESSAGE_TEXT).exists();
    assert.dom(ERROR_MESSAGE_TEXT).hasText(TEXT);
  });

});
