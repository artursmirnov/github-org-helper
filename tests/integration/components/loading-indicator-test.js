import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';

import { LOADING_INDICATOR } from '../../helpers/selectors';

module('Integration | Component | loading-indicator', function(hooks) {
  setupRenderingTest(hooks);

  test('it renders', async function(assert) {
    await render(hbs`{{loading-indicator value=10 }}`);

    assert.dom(LOADING_INDICATOR).exists();
  });
});
