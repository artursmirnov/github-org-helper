import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';

import { REPO_BRANCH } from '../../helpers/selectors';

module('Integration | Component | repo-branch', function(hooks) {
  setupRenderingTest(hooks);

  test('it renders', async function(assert) {
    await render(hbs`{{repo-branch}}`);

    assert.dom(REPO_BRANCH).exists();
  });
});
