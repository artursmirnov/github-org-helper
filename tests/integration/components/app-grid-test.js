import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';

import { APP_GRID } from '../../helpers/selectors';

module('Integration | Component | app-grid', function(hooks) {
  setupRenderingTest(hooks);

  test('it renders', async function(assert) {
    await render(hbs`{{app-grid}}`);

    assert.dom(APP_GRID).exists();
  });
});
