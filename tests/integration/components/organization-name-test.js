import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';

import { ORGANIZATION_NAME } from '../../helpers/selectors';

module('Integration | Component | organization-name', function(hooks) {
  setupRenderingTest(hooks);

  test('it renders', async function(assert) {
    await render(hbs`{{organization-name}}`);

    assert.dom(ORGANIZATION_NAME).exists();
  });
});
