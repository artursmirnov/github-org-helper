export const NO_DATA = '[data-test-no-data]';
export const AUTH_FORM_TRIGGER = '[data-test-auth-form-trigger]';
export const APP_GRID = '[data-test-app-grid]';
export const APP_HEADER = '[data-test-app-header]';
export const REPO_BRANCH = '[data-test-repo-branch]';
export const REPOSITORY_HEADER = '[data-test-repository-header]';
export const LOADING_INDICATOR = '[data-test-loading-indicator]';
export const REPOSITORY_FILTERS = '[data-test-repository-filters]';

export const ORGANIZATION_NAME = '[data-test-organization-name]';
export const ORGANIZATION_NAME_POPUP = '[data-test-organization-name-popup]';
export const ORGANIZATION_NAME_INPUT = '[data-test-organization-name-input] input';
export const ORGANIZATION_NAME_SUBMIT = '[data-test-organization-name-submit]';
export const ORGANIZATION_NAME_CANCEL = '[data-test-organization-name-cancel]';

export const ERROR_MESSAGE = '[data-test-error-message]';
export const ERROR_MESSAGE_TITLE = '[data-test-error-message-title]';
export const ERROR_MESSAGE_IMAGE = '[data-test-error-message-image]';
export const ERROR_MESSAGE_TEXT = '[data-test-error-message-text]';
