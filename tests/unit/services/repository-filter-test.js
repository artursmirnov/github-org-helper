import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';
import { faker } from 'ember-cli-mirage';

import { DEFAULT_FILTERS } from 'github-org-helper/services/repository-filter';

module('Unit | Service | repository-filter', function(hooks) {
  setupTest(hooks);

  let service;

  hooks.beforeEach(function() {
    service = this.owner.lookup('service:repository-filter');
    service.reset();
  });

  test('it exists', function(assert) {
    assert.ok(service);
  });

  test('it has correct properties', (assert) => {
    assert.equal(service.showPrivate, DEFAULT_FILTERS.showPrivate);
    assert.equal(service.showPublic, DEFAULT_FILTERS.showPublic);
    assert.equal(service.showLanguage, DEFAULT_FILTERS.showLanguage);
  });

  test('it applies new values', (assert) => {
    service.set('showPrivate', !DEFAULT_FILTERS.showPrivate);
    assert.notEqual(service.showPrivate, DEFAULT_FILTERS.showPrivate);

    service.set('showPublic', !DEFAULT_FILTERS.showPublic);
    assert.notEqual(service.showPublic, DEFAULT_FILTERS.showPublic);

    service.set('showLanguage', faker.lorem.word());
    assert.notEqual(service.showLanguage, DEFAULT_FILTERS.showLanguage);
  });

  test('it resets correctly', (assert) => {
    service.set('showPrivate', !DEFAULT_FILTERS.showPrivate);
    service.set('showPublic', !DEFAULT_FILTERS.showPublic);
    service.set('showLanguage', faker.lorem.word());
    service.reset();
    assert.equal(service.showPrivate, DEFAULT_FILTERS.showPrivate);
    assert.equal(service.showPublic, DEFAULT_FILTERS.showPublic);
    assert.equal(service.showLanguage, DEFAULT_FILTERS.showLanguage);
  });

});

