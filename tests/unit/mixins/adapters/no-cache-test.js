import EmberObject from '@ember/object';
import AdaptersNoCacheMixin from 'github-org-helper/mixins/adapters/no-cache';
import { module, test } from 'qunit';

module('Unit | Mixin | adapters/no-cache', function() {

  test('it works', function (assert) {
    let AdaptersNoCacheObject = EmberObject.extend(AdaptersNoCacheMixin);
    let subject = AdaptersNoCacheObject.create();
    assert.ok(subject);
    assert.equal(subject.ajaxOptions('/test', 'GET', {}).cache, false);
  });

});
